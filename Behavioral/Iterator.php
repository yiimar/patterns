<?php

namespace patterns\Behavioral;

/**
 * Итератор, Cursor (Iterator)
 * Представляет собой объект, позволяющий получить последовательный доступ к
 * элементам объекта-агрегата без использования описаний каждого из объектов,
 * входящих в состав агрегации.
 * Представляет способ доступа к элементам объекта без показа базового представления.
 *
 * пример телевизора, где вы можете нажимать кнопки следующего или предыдущего канала
 * для перехода через последовательные каналы, или, иными словами, они предоставляют
 * интерфейс для итерирования между соответствующими каналами, песнями или радиостанциями.
 *
 * В PHP очень просто реализовать это, используя SPL (Standard PHP Library). Приводя наш
 * пример с радиостанциями, изначально у нас есть Radiostation:
 */
class RadioStation
{
    protected $frequency;

    public function __construct(float $frequency)
    {
        $this->frequency = $frequency;
    }

    public function getFrequency(): float
    {
        return $this->frequency;
    }
}

/**
 * Затем у нас есть итератор:
 **/
use Countable;
use Iterator;

class StationList implements Countable, Iterator
{
    /** @var RadioStation[] $stations */
    protected $stations = [];

    /** @var int $counter */
    protected $counter;

    public function addStation(RadioStation $station)
    {
        $this->stations[] = $station;
    }

    public function removeStation(RadioStation $toRemove)
    {
        $toRemoveFrequency = $toRemove->getFrequency();
        $this->stations = array_filter($this->stations, function (RadioStation $station) use ($toRemoveFrequency) {
            return $station->getFrequency() !== $toRemoveFrequency;
        });
    }

    public function count(): int
    {
        return count($this->stations);
    }

    public function current(): RadioStation
    {
        return $this->stations[$this->counter];
    }

    public function key()
    {
        return $this->counter;
    }

    public function next()
    {
        $this->counter++;
    }

    public function rewind()
    {
        $this->counter = 0;
    }

    public function valid(): bool
    {
        return isset($this->stations[$this->counter]);
    }
}

/**
 * Пример использования:
 **/
$stationList = new StationList();

// Добавление станций
$stationList->addStation(new RadioStation(89));
$stationList->addStation(new RadioStation(101));
$stationList->addStation(new RadioStation(102));
$stationList->addStation(new RadioStation(103.2));

foreach($stationList as $station) {
    echo $station->getFrequency() . PHP_EOL;
}

$stationList->removeStation(new RadioStation(89)); // Удалит 89 станцию