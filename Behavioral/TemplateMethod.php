<?php

namespace patterns\Behavioral;

/**
 * Шаблонный метод (Template method)
 * Определяет основу алгоритма и позволяет наследникам переопределять некоторые
 * шаги алгоритма, не изменяя его структуру в целом.
 * Шаблонный метод определяет каркас выполнения определённого алгоритма,
 * но реализацию самих этапов делегирует дочерним классам.
 *
 * Допустим, у нас есть программный инструмент, позволяющий тестировать, проводить
 * контроль качества кода, выполнять сборку, генерировать отчёты сборки (отчёты о
 * покрытии кода, о качестве кода и т. д.), а также развёртывать приложение на тестовом сервере.
 */
 
 /**
  * Изначально у нас есть наш Builder, который описывает скелет для построения алгоритма:
  **/
abstract class Builder
{

    // Шаблонный метод
    final public function build()
    {
        $this->test();
        $this->lint();
        $this->assemble();
        $this->deploy();
    }

    abstract public function test();
    abstract public function lint();
    abstract public function assemble();
    abstract public function deploy();
}

/**
 * Затем у нас есть его реализации:
 **/
class AndroidBuilder extends Builder
{
    public function test()
    {
        echo 'Запуск Android тестов';
    }

    public function lint()
    {
        echo 'Копирование Android кода';
    }

    public function assemble()
    {
        echo 'Android сборка';
    }

    public function deploy()
    {
        echo 'Развертывание сборки на сервере';
    }
}

class IosBuilder extends Builder
{
    public function test()
    {
        echo 'Запуск iOS тестов';
    }

    public function lint()
    {
        echo 'Копирование iOS кода';
    }

    public function assemble()
    {
        echo 'iOS сборка';
    }

    public function deploy()
    {
        echo 'Развертывание сборки на сервере';
    }
}

/**
 * Пример использования:
 **/
$androidBuilder = new AndroidBuilder();
$androidBuilder->build();

// Вывод:
// Запуск Android тестов
// Копирование Android кода
// Android сборка
// Развертывание сборки на сервере

$iosBuilder = new IosBuilder();
$iosBuilder->build();

// Вывод:
// Запуск iOS тестов
// Копирование iOS кода
// iOS сборка
// Развертывание сборки на сервере