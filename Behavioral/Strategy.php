<?php

namespace patterns\Behavioral;

/**
 * Стратегия (Strategy)
 * Предназначен для определения семейства алгоритмов, инкапсуляции каждого из
 * них и обеспечения их взаимозаменяемости.
 * Шаблон стратегия позволяет переключаться между алгоритмами или стратегиями в зависимости от ситуации.
 *
 * пример с пузырьковой сортировкой. Мы её реализовали, но с ростом объёмов данных сортировка работа
 * стала выполняться очень медленно. Тогда мы сделали быструю сортировку. Алгоритм работает быстрее на
 * больших объёмах, но на маленьких он очень медленный. Тогда мы реализовали стратегию, при которой для
 * маленьких объёмов данных используется пузырьковая сортировка, а для больших объёмов — быстрая.
 */
 
 /**
  * Изначально у нас есть наша SortStrategy и разные её реализации:
  **/
interface SortStrategy
{
    public function sort(array $dataset): array;
}

class BubbleSortStrategy implements SortStrategy
{
    public function sort(array $dataset): array
    {
        echo "Сортировка пузырьком";

        // Сортировка
        return $dataset;
    }
}

class QuickSortStrategy implements SortStrategy
{
    public function sort(array $dataset): array
    {
        echo "Быстрая сортировка";

        // Сортировка
        return $dataset;
    }
}

/**
 * И у нас есть Sorter, который собирается использовать какую-то стратегию:
 **/
class Sorter
{
    protected $sorter;

    public function __construct(SortStrategy $sorter)
    {
        $this->sorter = $sorter;
    }

    public function sort(array $dataset): array
    {
        return $this->sorter->sort($dataset);
    }
}

/**
 * Пример использования:
 **/
$dataset = [1, 5, 4, 3, 2, 8];

$sorter = new Sorter(new BubbleSortStrategy());
$sorter->sort($dataset); // Вывод : Сортировка пузырьком

$sorter = new Sorter(new QuickSortStrategy());
$sorter->sort($dataset); // Вывод : Быстрая сортировка