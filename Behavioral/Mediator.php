<?php

namespace patterns\Behavioral;

/**
 * Посредник (Mediator)
 * Обеспечивает взаимодействие множества объектов, формируя при этом слабую
 * связанность и избавляя объекты от необходимости явно ссылаться друг на друга.
 * Шаблон посредник подразумевает добавление стороннего объекта (посредника) для
 * управления взаимодействием между двумя объектами (коллегами). Шаблон помогает
 * уменьшить связанность (coupling) классов, общающихся друг с другом, ведь теперь
 * они не должны знать о реализациях своих собеседников.
 * 
 * Простейший пример: чат (посредник), в котором пользователи (коллеги) отправляют друг другу сообщения.
 * Изначально у нас есть посредник ChatRoomMediator:
 */
interface ChatRoomMediator 
{
    public function showMessage(User $user, string $message);
}

// Посредник
class ChatRoom implements ChatRoomMediator
{
    public function showMessage(User $user, string $message)
    {
        $time = date('M d, y H:i');
        $sender = $user->getName();

        echo $time . '[' . $sender . ']:' . $message;
    }
}

/**
 * Затем у нас есть наши User (коллеги):
 **/
class User {
protected $name;
    protected $chatMediator;

    public function __construct(string $name, ChatRoomMediator $chatMediator) {
        $this->name = $name;
        $this->chatMediator = $chatMediator;
    }

    public function getName() {
        return $this->name;
    }

    public function send($message) {
        $this->chatMediator->showMessage($this, $message);
    }
}

/**
 * Пример использования:
 **/
$mediator = new ChatRoom();

$john = new User('John Doe', $mediator);
$jane = new User('Jane Doe', $mediator);

$john->send('Привет!');
$jane->send('Привет!');

// Вывод
// Feb 14, 10:58 [John]: Привет!
// Feb 14, 10:58 [Jane]: Привет!