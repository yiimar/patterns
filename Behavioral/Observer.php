<?php

namespace patterns\Behavioral;

/**
 * Наблюдатель или Издатель-подписчик (Observer)
 * Определяет зависимость типа «один ко многим» между объектами таким образом,
 * что при изменении состояния одного объекта все зависящие от него оповещаются
 * об этом событии.
 * Шаблон определяет зависимость между объектами, чтобы при изменении состояния
 * одного из них зависимые от него узнавали об этом.
 *
 * Пример: люди, ищущие работу, подписываются на публикации на сайтах вакансий
 * и получают уведомления, когда появляются вакансии подходящие по параметрам.
 */
 
 /**
  * Изначально у нас есть JobSeeker, которые ищут работы JobPost и должны быть
  * уведомлены о её появлении:
  **/
class JobPost
{
    protected $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }
}

interface Observer
{
    public function onJobPosted(JobPost $job);
}

class JobSeeker implements Observer
{
    protected $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function onJobPosted(JobPost $job)
    {
        // Делаем что-то с публикациями вакансий
        echo 'Привет ' . $this->name . '! Появилась новая работа: '. $job->getTitle();
    }
}

/**
 * Затем мы делаем публикации JobPostings на которые соискатели могут подписываться:
 **/

interface Observable
{
    public function attach(Observer $observer);
    public function addJob(JobPost $jobPosting);
    protected function notify(JobPost $jobPosting);
}

class JobPostings implements Observable
{
    protected $observers = [];

    protected function notify(JobPost $jobPosting)
    {
        foreach ($this->observers as $observer) {
            $observer->onJobPosted($jobPosting);
        }
    }

    public function attach(Observer $observer)
    {
        $this->observers[] = $observer;
    }

    public function addJob(JobPost $jobPosting)
    {
        $this->notify($jobPosting);
    }
}

/**
 * Пример использования:
 **/
// Создаем соискателей
$johnDoe = new JobSeeker('John Doe');
$janeDoe = new JobSeeker('Jane Doe');

// Создаем публикацию и добавляем подписчика
$jobPostings = new JobPostings();
$jobPostings->attach($johnDoe);
$jobPostings->attach($janeDoe);

// Добавляем новую работу и смотрим получит ли соискатель уведомление
$jobPostings->addJob(new JobPost('Software Engineer'));

// Вывод
// Привет John Doe! Появилась новая работа: Software Engineer
// Привет Jane Doe! Появилась новая работа: Software Engineer