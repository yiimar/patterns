<?php

namespace patterns\Behavioral;

/**
 * Одноразовый посетитель (Single-serving visitor)
 * Оптимизирует реализацию шаблона посетитель, который инициализируется,
 *  единожды используется, и затем удаляется.
 *
 * @author yiimar
 */
class SingleServingVisitor
{
    //put your code here
}