<?php

namespace patterns\Behavioral;

/**
 * Состояние (State)
 * Используется в тех случаях, когда во время выполнения программы объект должен
 * менять своё поведение в зависимости от своего состояния.
 * Шаблон позволяет менять поведение класса при изменении состояния.
 *
 * Возьмем пример текстового редактора, он позволяет вам менять состояние напечатанного текста.
 * Например, если у вас выбран курсив, то он будет писать курсивом и так далее.
 */
 
 /**
  * Изначально у нас есть интерфейс WritingState и несколько его реализаций:
  **/
interface WritingState
{
    public function write(string $words);
}

class UpperCase implements WritingState
{
    public function write(string $words)
    {
        echo strtoupper($words);
    }
}

class LowerCase implements WritingState
{
    public function write(string $words)
    {
        echo strtolower($words);
    }
}

class Default implements WritingState
{
    public function write(string $words)
    {
        echo $words;
    }
}

/**
 * Затем TextEditor:
 **/
class TextEditor
{
    protected $state;

    public function __construct(WritingState $state)
    {
        $this->state = $state;
    }

    public function setState(WritingState $state)
    {
        $this->state = $state;
    }

    public function type(string $words)
    {
        $this->state->write($words);
    }
}

/**
 * Пример использования:
 **/
$editor = new TextEditor(new Default());

$editor->type('Первая строка');

$editor->setState(new UpperCase());

$editor->type('Вторая строка');
$editor->type('Третья строка');

$editor->setState(new LowerCase());

$editor->type('Четвертая строка');
$editor->type('Пятая строка');

// Output:
// Первая строка
// ВТОРАЯ СТРОКА
// ТРЕТЬЯ СТРОКА
// четвертая строка
// пятая строка