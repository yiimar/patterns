<?php

namespace patterns\Behavioral;

/**
 * Хранитель (Memento)
 * Позволяет не нарушая инкапсуляцию зафиксировать и сохранить внутренние
 * состояния объекта так, чтобы позднее восстановить его в этих состояниях.
 * Шаблон хранитель фиксирует и хранит текущее состояние объекта, чтобы оно легко восстанавливалось.
 *
 * пример текстового редактора, который время от времени сохраняет состояние, которое вы можете восстановить.
 */
 
 /**
  * Изначально у нас есть наш объект EditorMemento, который может содержать состояние редактора:
  **/
class EditorMemento
{
    protected $content;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    public function getContent()
    {
        return $this->content;
    }
}

/**
 * Затем у нас есть наш Editor (создатель), который будет использовать объект хранитель:
 **/
class Editor
{
    protected $content = '';

    public function type(string $words)
    {
        $this->content = $this->content . ' ' . $words;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function save()
    {
        return new EditorMemento($this->content);
    }

    public function restore(EditorMemento $memento)
    {
        $this->content = $memento->getContent();
    }
}

/**
 * Пример использования:
 **/
$editor = new Editor();

// Печатаем что-нибудь
$editor->type('Это первое предложение.');
$editor->type('Это второе.');

// Сохраняем состояние для восстановления : Это первое предложение. Это второе.
$saved = $editor->save();

// Печатаем ещё
$editor->type('И это третье.');

// Вывод: Данные до сохранения
echo $editor->getContent(); // Это первое предложение. Это второе. И это третье.

// Восстановление последнего сохранения
$editor->restore($saved);

$editor->getContent(); // Это первое предложение. Это второе.