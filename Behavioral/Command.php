<?php

namespace patterns\Behavioral;

/**
 * Команда (Action, Transaction Command)
 * Представляет действие. Объект команды заключает в себе само действие и его параметры.
 * озволяет вам инкапсулировать действия в объекты. Основная идея, стоящая за шаблоном — 
 * это предоставление средств, для разделения клиента и получателя.
 *
 * Шаблон команда может быть использован для реализации системы, основанной на транзакциях,
 * где вы сохраняете историю команд, как только их выполняете. Если окончательная команда
 * успешно выполнена, то все хорошо, иначе алгоритм просто перебирает историю и продолжает
 * выполнять отмену для всех выполненных команд.
 *
 * Типичный пример:
 *   вы заказываете еду в ресторане. Вы (т.е. Client) просите официанта (например, Invoker)
 *   принести еду (то есть Command), а официант просто переправляет запрос шеф-повару (то
 *   есть Receiver), который знает, что и как готовить. Другим примером может быть то, что
 *   вы (Client) включаете (Command) телевизор (Receiver) с помощью пульта дистанционного
 *   управления (Invoker).
 */
 /**
  * Изначально у нас есть получатель Bulb, в котором есть реализация каждого действия,
  * которое может быть выполнено:
  **/
// Получатель
class Bulb
{
    public function turnOn()
    {
        echo "Лампочка загорелась";
    }

    public function turnOff()
    {
        echo "Темнота!";
    }
}

/**
 * Затем у нас есть интерфейс Command, которая каждая команда должна реализовывать,
 * и затем у нас будет набор команд:
  **/
  interface Command
{
    public function execute();
    public function undo();
    public function redo();
}

// Команда
class TurnOn implements Command
{
    protected $bulb;

    public function __construct(Bulb $bulb)
    {
        $this->bulb = $bulb;
    }

    public function execute()
    {
        $this->bulb->turnOn();
    }

    public function undo()
    {
        $this->bulb->turnOff();
    }

    public function redo()
    {
        $this->execute();
    }
}

class TurnOff implements Command
{
    protected $bulb;

    public function __construct(Bulb $bulb)
    {
        $this->bulb = $bulb;
    }

    public function execute()
    {
        $this->bulb->turnOff();
    }

    public function undo()
    {
        $this->bulb->turnOn();
    }

    public function redo()
    {
        $this->execute();
    }
}

/**
 * Затем у нас есть Invoker, с которым клиент будет взаимодействовать для обработки любых команд:
 **/
// Invoker
class RemoteControl
{
    public function submit(Command $command)
    {
        $command->execute();
    }
}

/**
 * как использовать нашего клиента:
 **/
$bulb = new Bulb();

$turnOn = new TurnOn($bulb);
$turnOff = new TurnOff($bulb);

$remote = new RemoteControl();
$remote->submit($turnOn); // Лампочка загорелась!
$remote->submit($turnOff); // Темнота! 