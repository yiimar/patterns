<?php

namespace patterns\Behavioral;

/**
 * Цепочка обязанностей (Chain of responsibility)
 * Предназначен для организации в системе уровней ответственности.
 * Цепочка обязанностей помогает строить цепочки объектов. Запрос входит
 * с одного конца и проходит через каждый объект, пока не найдет подходящий обработчик.
 *
 * @author Камран Ахмед
 * Пример с банковскими счетами:
 *   у вас есть три платежных метода (A, B и C), настроенных на вашем банковском счёте.
 *   На каждом лежит разное количество денег. На A есть 100 долларов, на B есть 300 долларов
 *   и на C — 1000 долларов. Предпочтение отдается в следующем порядке: A, B и C. Вы пытаетесь
 *   заказать что-то, что стоит 210 долларов. Используя цепочку обязанностей, первым на возможность
 *   оплаты будет проверен метод А, и в случае успеха пройдет оплата и цепь разорвется. Если нет,
 *   то запрос перейдет к методу B для аналогичной проверки. Здесь A, B и C — это звенья цепи,
 *   а все явление — цепочка обязанностей.
 */
abstract class Account
{
    protected $successor;
    protected $balance;

    public function setNext(Account $account)
    {
        $this->successor = $account;
    }

    public function pay(float $amountToPay)
    {
        if ($this->canPay($amountToPay)) {
            echo sprintf('Оплата %s, используя %s' . PHP_EOL, $amountToPay, get_called_class());
        } elseif ($this->successor) {
            echo sprintf('Нельзя заплатить, используя %s. Обработка ..' . PHP_EOL, get_called_class());
            $this->successor->pay($amountToPay);
        } else {
            throw new Exception('Ни на одном из аккаунтов нет необходимого количества денег');
        }
    }

    public function canPay($amount): bool
    {
        return $this->balance >= $amount;
    }
}

class Bank extends Account
{
    protected $balance;

    public function __construct(float $balance)
    {
        $this->balance = $balance;
    }
}

class Paypal extends Account
{
    protected $balance;

    public function __construct(float $balance)
    {
        $this->balance = $balance;
    }
}

class Bitcoin extends Account
{
    protected $balance;

    public function __construct(float $balance)
    {
        $this->balance = $balance;
    }
}

/**
 * Теперь приготовим цепь, используя объявленные выше звенья (например, Bank, Paypal, Bitcoin):
 **/
// Подготовим цепь
//      $bank->$paypal->$bitcoin
//
// Первый по приоритету банк
//      Если нельзя через банк, то Paypal
//      Если нельзя через Paypal, то Bitcoin

$bank = new Bank(100);          // Банк с балансом 100
$paypal = new Paypal(200);      // Paypal с балансом 200
$bitcoin = new Bitcoin(300);    // Bitcoin с балансом 300

$bank->setNext($paypal);
$paypal->setNext($bitcoin);

// Попробуем оплатить через банк
$bank->pay(259);

// Вывод
// ==============
// Нельзя заплатить, используя Банк. Обработка ..
// Нельзя заплатить, используя Paypal. Обработка ..:
// Оплата 259, используя Bitcoin!