# Шаблоны проектирования параллельного программирования

Перечень
--------

  - Active Object
  - Balking pattern
  - Barrier
  - Double-checked locking
  - Guarded suspension
  - Leaders/followers pattern
  - Monitor Object
  - Nuclear reaction
  - Reactor pattern
  - Read write lock pattern
  - Scheduler pattern
  - Thread pool pattern
  - Thread-local storage

Использование
-------------

Эти шаблоны используется для более эффективного написания многопоточных программ, и предоставляет готовые решения проблем синхронизации.

