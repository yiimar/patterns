<?php

namespace pattens\Fundamental;

/**
 * Общий метод для структурирования компьютерных программ для того,
 * чтобы их было проще понять.
 *
 * @author yiimar
 */
interface IInterface
{
    function doSomething($param1);
}