<?php

namespace patterns\Creational;

/**
 * Получение ресурса есть инициализация (Resource acquisition is initialization (RAII) )
 * Получение некоторого ресурса совмещается с инициализацией, а освобождение —
 * с уничтожением объекта.
 *
 * @author yiimar
 */
class RAII
{
    //put your code here
}