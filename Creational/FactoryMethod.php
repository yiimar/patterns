<?php

namespace patterns\Creational;

/**
 * Фабричный метод (Factory method)
 * Определяет интерфейс для создания объекта, но оставляет подклассам решение
 * о том, какой класс инстанцировать.
 * данный шаблон делегирует создание объектов наследникам родительского класса.
 * Это позволяет использовать в коде программы не специфические классы, а
 * манипулировать абстрактными объектами на более высоком уровне.
 * Менеджер предоставляет способ делегирования логики создания экземпляра дочерним классам.
 *
 * пример с менеджером по найму. Невозможно одному человеку провести собеседования
 * со всеми кандидатами на все вакансии. В зависимости от вакансии он должен
 * распределить этапы собеседования между разными людьми.
 */
/**
 * Изначально у нас есть интерфейс Interviewer и несколько реализаций для него:
 **/ 
interface Interviewer
{
    public function askQuestions();
}

class Developer implements Interviewer
{
    public function askQuestions()
    {
        echo 'Спрашивает про шаблоны проектирования!';
    }
}

class CommunityExecutive implements Interviewer
{
    public function askQuestions()
    {
        echo 'Спрашивает о работе с сообществом';
    }
}

/**
 * Теперь создадим нашего HiringManager:
 **/
abstract class HiringManager
{

    // Фабричный метод
    abstract public function makeInterviewer(): Interviewer;

    public function takeInterview()
    {
        $interviewer = $this->makeInterviewer();
        $interviewer->askQuestions();
    }
}

/**
 * И теперь любой дочерний класс может расширять его и предоставлять необходимого интервьюера:
 **/
class DevelopmentManager extends HiringManager
{
    public function makeInterviewer(): Interviewer
    {
        return new Developer();
    }
}

class MarketingManager extends HiringManager
{
    public function makeInterviewer(): Interviewer
    {
        return new CommunityExecutive();
    }
}

/**
 *Пример использования:
 **/
$devManager = new DevelopmentManager();
$devManager->takeInterview(); // Вывод: Спрашивает о шаблонах проектирования!

$marketingManager = new MarketingManager();
$marketingManager->takeInterview(); // Вывод: Спрашивает о работе с сообществом
/**
 * Когда использовать: Полезен, когда есть некоторая общая обработка в классе,
 * но необходимый подкласс динамически определяется во время выполнения. Иными
 * словами, когда клиент не знает, какой именно подкласс ему может понадобиться.
 **/
