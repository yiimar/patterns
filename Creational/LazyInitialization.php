<?php

namespace patterns\Creational;

/**
 * Отложенная инициализация (Lazy initialization)
 * Объект, инициализируемый во время первого обращения к нему.
 *
 * @author yiimar
 */
class LazyInitialization
{
    //put your code here
}