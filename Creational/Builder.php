<?php

namespace patterns\Creational;

/**
 * Строитель (Builder)
 * Класс, который представляет собой интерфейс для создания составного объекта.
 * Предназначен для решения проблемы антипаттерна «Телескопический конструктор».
 * Шаблон позволяет вам создавать различные виды объекта, избегая засорения
 * конструктора. Он полезен, когда может быть несколько видов объекта или когда
 * необходимо множество шагов, связанных с его созданием.
 *
 * что такое «Телескопический конструктор». Когда-то мы все видели конструктор вроде такого:
 */
public function __construct($size, $cheese = true, $pepperoni = true, $tomato = false, $lettuce = true)
{
}
// Как вы можете заметить, количество параметров конструктора может резко увеличиться,
// и станет сложно понимать расположение параметров. Кроме того, этот список параметров
// будет продолжать расти, если вы захотите добавить новые варианты. Это и есть
// «Телескопический конструктор».

// Перейдем к примеру в коде. Адекватной альтернативой будет использование шаблона «Строитель».
// Сначала у нас есть Burger, который мы хотим создать:
class Burger
{
    protected $size;

    protected $cheese = false;
    protected $pepperoni = false;
    protected $lettuce = false;
    protected $tomato = false;

    public function __construct(BurgerBuilder $builder)
    {
        $this->size = $builder->size;
        $this->cheese = $builder->cheese;
        $this->pepperoni = $builder->pepperoni;
        $this->lettuce = $builder->lettuce;
        $this->tomato = $builder->tomato;
    }
}

// Затем мы берём «Строителя»:

class BurgerBuilder
{
    public $size;

    public $cheese = false;
    public $pepperoni = false;
    public $lettuce = false;
    public $tomato = false;

    public function __construct(int $size)
    {
        $this->size = $size;
    }

    public function addPepperoni()
    {
        $this->pepperoni = true;
        return $this;
    }

    public function addLettuce()
    {
        $this->lettuce = true;
        return $this;
    }

    public function addCheese()
    {
        $this->cheese = true;
        return $this;
    }

    public function addTomato()
    {
        $this->tomato = true;
        return $this;
    }

    public function build(): Burger
    {
        return new Burger($this);
    }
}

// Пример использования:
$burger = (new BurgerBuilder(14))
                    ->addPepperoni()
                    ->addLettuce()
                    ->addTomato()
                    ->build();
// Когда использовать: Когда может быть несколько видов объекта и надо
// избежать «телескопического конструктора». Главное отличие от «фабрики» —
// это то, что она используется, когда создание занимает один шаг, а
// «строитель» применяется при множестве шагов.