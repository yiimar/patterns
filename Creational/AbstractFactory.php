<?php

namespace patterns\Creational;

/**
 * Абстрактная фабрика (Abstract factory)
 * Класс, который представляет собой интерфейс для создания компонентов системы.
 *  Шаблон реализуется созданием абстрактного класса Factory, который представляет
 * собой интерфейс для создания компонентов системы (например, для оконного интерфейса
 * он может создавать окна и кнопки). Затем пишутся классы, реализующие этот интерфейс.
 * Фабрика фабрик. Фабрика, которая группирует индивидуальные, но связанные/зависимые
 * фабрики без указания их конкретных классов.
 *
 * Расширим наш пример про двери из простой фабрики. В зависимости от ваших нужд вам
 * понадобится деревянная дверь из одного магазина, железная дверь — из другого или
 * пластиковая — из третьего. Кроме того, вам понадобится соответствующий специалист:
 * столяр для деревянной двери, сварщик для железной двери и так далее. Как вы можете
 * заметить, тут есть зависимость между дверьми.
 */
/**
 * Используем пример про двери. Сначала у нас есть интерфейс Door и несколько его реализаций:
 *//
interface Door
{
    public function getDescription();
}

class WoodenDoor implements Door
{
    public function getDescription()
    {
        echo 'Я деревянная дверь';
    }
}

class IronDoor implements Door
{
    public function getDescription()
    {
        echo 'Я железная дверь';
    }
}

/**
 * Затем у нас есть несколько DoorFittingExpert для каждого типа дверей:
 **/
interface DoorFittingExpert
{
    public function getDescription();
}

class Welder implements DoorFittingExpert
{
    public function getDescription()
    {
        echo 'Я работаю только с железными дверьми';
    }
}

class Carpenter implements DoorFittingExpert
{
    public function getDescription()
    {
        echo 'Я работаю только с деревянными дверьми';
    }
}

/**
 * Теперь у нас есть DoorFactory, которая позволит нам создать семейство связанных объектов. То есть фабрика деревянных дверей предоставит нам деревянную дверь и эксперта по деревянным дверям. Аналогично для железных дверей:
 **/
interface DoorFactory
{
    public function makeDoor(): Door;
    public function makeFittingExpert(): DoorFittingExpert;
}

// Деревянная фабрика вернет деревянную дверь и столяра
class WoodenDoorFactory implements DoorFactory
{
    public function makeDoor(): Door
    {
        return new WoodenDoor();
    }

    public function makeFittingExpert(): DoorFittingExpert
    {
        return new Carpenter();
    }
}

// Железная фабрика вернет железную дверь и сварщика
class IronDoorFactory implements DoorFactory
{
    public function makeDoor(): Door
    {
        return new IronDoor();
    }

    public function makeFittingExpert(): DoorFittingExpert
    {
        return new Welder();
    }
}

/**
 * Пример использования:
 **/
$woodenFactory = new WoodenDoorFactory();

$door = $woodenFactory->makeDoor();
$expert = $woodenFactory->makeFittingExpert();

$door->getDescription();  // Вывод: Я деревянная дверь
$expert->getDescription(); // Вывод: Я работаю только с деревянными дверями

// Аналогично для железной двери
$ironFactory = new IronDoorFactory();

$door = $ironFactory->makeDoor();
$expert = $ironFactory->makeFittingExpert();

$door->getDescription();  // Вывод: Я железная дверь
$expert->getDescription(); // Вывод: Я работаю только с железными дверями
/**
 * Как вы можете заметить, фабрика деревянных дверей инкапсулирует столяра
 * и деревянную дверь, а фабрика железных дверей инкапсулирует железную дверь
 * и сварщика. Это позволило нам убедиться, что для каждой двери мы получим
 * нужного нам эксперта.
 *
 * Когда использовать: Когда есть взаимосвязанные зависимости с не очень простой
 * логикой создания.