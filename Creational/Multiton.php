<?php

namespace patterns\Creational;

/**
 * Пул одиночек (Multiton)
 * Гарантирует, что класс имеет поименованные экземпляры объекта и обеспечивает
 * глобальную точку доступа к ним.
 *
 * @author yiimar
 */
class Multiton
{
    //put your code here
}