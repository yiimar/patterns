/**
 * фабрика — это объект для создания других объектов. Формально фабрика — 
 * это функция или метод, который возвращает объекты изменяющегося прототипа
 * или класса из некоторого вызова метода, который считается «новым».
 * Простая фабрика генерирует экземпляр для клиента, не раскрывая никакой логики.
 *
 * надо построить дом, и вам нужны двери. Было бы глупо каждый раз, когда вам нужны
 * двери, надевать вашу столярную форму и начинать делать дверь.
 * Вместо этого вы делаете её на фабрике.
 **/
 /**
  * У нас есть интерфейс Door и его реализация:
  **/
interface Door
{
    public function getWidth(): float;
    public function getHeight(): float;
}

class WoodenDoor implements Door
{
    protected $width;
    protected $height;

    public function __construct(float $width, float $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    public function getWidth(): float
    {
        return $this->width;
    }

    public function getHeight(): float
    {
        return $this->height;
    }
}

/**
 * Затем у нас есть наша DoorFactory, которая делает дверь и возвращает её:
 **/
class DoorFactory
{
    public static function makeDoor($width, $height): Door
    {
        return new WoodenDoor($width, $height);
    }
}

/**
 * И затем мы можем использовать всё это:
 **/
$door = DoorFactory::makeDoor(100, 200);
echo 'Width: ' . $door->getWidth();
echo 'Height: ' . $door->getHeight();

/**
 * Когда использовать: Когда создание объекта — это не просто несколько присвоений,
 * а какая-то логика, тогда имеет смысл создать отдельную фабрику вместо повторения
 * одного и того же кода повсюду.
 **/