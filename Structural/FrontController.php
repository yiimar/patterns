<?php

namespace patterns\Structural;

/**
 * Единая точка входа (Front controller)
 * Обеспечивает унифицированный интерфейс для интерфейсов в подсистеме.
 * Front Controller определяет высокоуровневый интерфейс, упрощающий использование
 * подсистемы.
 *
 * @author yiimar
 */
class FrontController
{
    //put your code here
}