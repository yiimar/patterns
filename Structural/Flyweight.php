<?php

namespace patterns\Structural;

/**
 * Приспособленец (Flyweight)
 * Это объект, представляющий себя как уникальный экземпляр в разных местах
 * программы, но фактически не являющийся таковым
 * Приспособленец используется для минимизации использования памяти или
 * вычислительной стоимости путем разделения ресурсов с наибольшим количеством
 * похожих объектов.
 *
 * Возьмем наш пример с чаем.
 */

// Изначально у нас есть различные виды Tea и TeaMaker:

// Все, что будет закешировано, является приспособленцем.
// Типы чая здесь будут приспособленцами.
class KarakTea
{
}

//Ведет себя как фабрика и сохраняет чай
class TeaMaker
{
    protected $availableTea = [];

    public function make($preference)
    {
        if (empty($this->availableTea[$preference])) {
            $this->availableTea[$preference] = new KarakTea();
        }

        return $this->availableTea[$preference];
    }
}

// Теперь у нас есть TeaShop, который принимает заказы и выполняет их:
class TeaShop
{
    protected $orders;
    protected $teaMaker;

    public function __construct(TeaMaker $teaMaker)
    {
        $this->teaMaker = $teaMaker;
    }

    public function takeOrder(string $teaType, int $table)
    {
        $this->orders[$table] = $this->teaMaker->make($teaType);
    }

    public function serve()
    {
        foreach ($this->orders as $table => $tea) {
            echo "Serving tea to table# " . $table;
        }
    }
}

// Пример использования:
$teaMaker = new TeaMaker();
$shop = new TeaShop($teaMaker);

$shop->takeOrder('меньше сахара', 1);
$shop->takeOrder('больше молока', 2);
$shop->takeOrder('без сахара', 5);

$shop->serve();
// Подаем чай на первый стол
// Подаем чай на второй стол
// Подаем чай на пятый стол