<?php

namespace patterns\Structural;

/**
 * Заместитель (Proxy)
 * Объект, который является посредником между двумя другими объектами, и который
 * реализует/ограничивает доступ к объекту, к которому обращаются через него.
 * Т.е. является суррогатом другого объекта и контролирует доступ к нему.
 * Используя шаблон заместитель, класс отображает функциональность другого класса.
 *
 * Стандартныеые применения
 *    - При использовании подхода DDD (Domain-driven design) при проектировании, для связки модели с маппером можно использовать прокси
 *    - Можно использовать прокси, как локальных представителей распределенных объектов
 *
 * Возьмем наш пример с безопасностью.
 */

// Сначала у нас есть интерфейс Door и его реализация:
interface Door
{
    public function open();
    public function close();
}

class LabDoor implements Door
{
    public function open()
    {
        echo "Открытие дверь лаборатории";
    }

    public function close()
    {
        echo "Закрытие двери лаборатории";
    }
}

// Затем у нас есть заместитель Security для защиты любых наших дверей:
class Security
{
    protected $door;

    public function __construct(Door $door)
    {
        $this->door = $door;
    }

    public function open($password)
    {
        if ($this->authenticate($password)) {
            $this->door->open();
        } else {
            echo "Нет! Это невозможно.";
        }
    }

    public function authenticate($password)
    {
        return $password === '$ecr@t';
    }

    public function close()
    {
        $this->door->close();
    }
}

// Пример использования:
$door = new Security(new LabDoor());
$door->open('invalid'); // Нет! Это невозможно.

$door->open('$ecr@t'); // Открытие двери лаборатории
$door->close(); // Закрытие двери лаборатории

// Другим примером будет реализация маппинга данных. Например, недавно я создал ODM
// (Object Data Mapper) для MongoDB, используя этот шаблон, где я написал заместитель
// вокруг классов mongo и использовал магический метод __call(). Все вызовы методов
// были замещены оригинальным классом mongo, и полученный результат возвращался без
// изменений, но в случае find или findOne данные сопоставлялись необходимому классу,
// и возвращались в объект вместо Cursor.