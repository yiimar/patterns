<?php

namespace patterns\Structural;

/**
 * Адаптер (Adapter / Wrapper)
 * Объект, обеспечивающий взаимодействие двух других объектов, один из которых
 * использует, а другой предоставляет несовместимый с первым интерфейс.
 * Шаблон позволяет обернуть несовместимые объекты в адаптер, чтобы сделать их
 * совместимыми с другим классом.
 *
 * Представим игру, в которой охотник охотится на львов.
 */
// Изначально у нас есть интерфейс Lion, который реализует всех львов:
interface Lion
{
    public function roar();
}

class AfricanLion implements Lion
{
    public function roar()
    {
    }
}

class AsianLion implements Lion
{
    public function roar()
    {
    }
}
И Hunter охотится на любую реализацию интерфейса Lion:

class Hunter
{
    public function hunt(Lion $lion)
    {
    }
}
// Теперь представим, что нам надо добавить WildDog в нашу игру, на которую наш
// Hunter также мог бы охотиться. Но мы не можем сделать это напрямую, потому что
// у WildDog другой интерфейс. Чтобы сделать её совместимой с нашим Hunter, нам
// надо создать адаптер:
// Это надо добавить в игру
class WildDog
{
    public function bark()
    {
    }
}

// Адаптер, чтобы сделать WildDog совместимой с нашей игрой
class WildDogAdapter implements Lion
{
    protected $dog;
    
    public function __construct(WildDog $dog)
    {
        $this->dog = $dog;
    }
    
    public function roar()
    {
        $this->dog->bark();
    }
}
// Способ применения:

$wildDog = new WildDog();
$wildDogAdapter = new WildDogAdapter($wildDog);

$hunter = new Hunter();
$hunter->hunt($wildDogAdapter);